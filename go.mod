module t8n.io/vaccinalert

go 1.13

require (
	github.com/messagebird/go-rest-api/v6 v6.1.0
	github.com/mmcdole/gofeed v1.1.3
	go.uber.org/zap v1.17.0
)
