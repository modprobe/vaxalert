package vaccinalert

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"

	messagebird "github.com/messagebird/go-rest-api/v6"
	"github.com/messagebird/go-rest-api/v6/sms"
	"go.uber.org/zap"
)

const BaseUrl = "https://user-api.coronatest.nl/vaccinatie/programma/bepaalbaar/%d/NEE/NEE"

func TriggerCheck(w http.ResponseWriter, r *http.Request) {
	err := Check()

	w.Header().Add("Content-Type", "application/json")

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprint(w, "{\"success\": false}")
		return
	}

	fmt.Fprint(w, "{\"success\": true}")
}

type Config struct {
	Recipient         string
	TargetYear        int
	MessagebirdApiKey string
}

func GetConfig() (*Config, error) {
	config := Config{}

	recipient, present := os.LookupEnv("VAX_RECIPIENT")
	if !present {
		return nil, fmt.Errorf("recipient config not in env")
	}

	config.Recipient = recipient

	targetYear, present := os.LookupEnv("VAX_TARGET_YEAR")
	if !present {
		return nil, fmt.Errorf("target year config not in env")
	}

	var err error
	config.TargetYear, err = strconv.Atoi(targetYear)
	if err != nil {
		return nil, err
	}

	mbKey, present := os.LookupEnv("VAX_MESSAGEBIRD_KEY")
	if !present {
		return nil, fmt.Errorf("messagebird AccessKey config not in env")
	}

	config.MessagebirdApiKey = mbKey

	return &config, nil
}

func Check() error {
	var z *zap.Logger

	if os.Getenv("VAX_ENV") == "prod" {
		z, _ = zap.NewProduction()
	} else {
		z, _ = zap.NewDevelopment()
	}

	defer z.Sync()
	logger := z.Sugar()

	config, err := GetConfig()
	if err != nil {
		logger.Errorw("Couldn't get config from env", "err", err)
		return err
	}

	resp, err := http.Get(fmt.Sprintf(BaseUrl, config.TargetYear))
	if err != nil {
		logger.Errorw("Error fetching API URL", "err", err)
		return err
	}

	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)

	var jsonResp struct {
		Success bool `json:"success"`
	}

	err = json.Unmarshal(body, &jsonResp)
	logger.Info(jsonResp)
	if err != nil {
		logger.Errorw("Couldn't parse JSON from API")
		return err
	}

	if !jsonResp.Success {
		logger.Infow("Target year not called yet", "targetYear", config.TargetYear)
		return nil
	}

	client := messagebird.New(config.MessagebirdApiKey)

	msg, err := sms.Create(
		client,
		"inbox",
		[]string{config.Recipient},
		fmt.Sprintf("%d called for vaccination. Appt: https://coronatest.nl/ik-wil-me-laten-vaccineren/een-online-afspraak-maken", config.TargetYear),
		nil,
	)

	if err != nil {
		logger.Errorw("Couldn't send SMS", "err", err)
		return err
	}

	logger.Infow("Sent notification SMS", "msg", msg)
	return nil
}
