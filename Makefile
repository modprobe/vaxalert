PROJECTNAME=$(shell basename "$(PWD)")

# Go related variables.
GOBASE=$(shell pwd)
GOPATH="$(GOBASE)/vendor:$(GOBASE)"
GOBIN=$(GOBASE)/bin
GOFILES=$(wildcard *.go)

GCP_REGION=europe-west3
FUNCTION_NAME=vaxalert
SCHEDULER_NAME=check-vax-year

# Redirect error output to a file, so we can show it in development mode.
STDERR=/tmp/.$(PROJECTNAME)-stderr.txt

# PID file will keep the process id of the server
PID=/tmp/.$(PROJECTNAME).pid

# Make is verbose in Linux. Make it silent.
MAKEFLAGS += --silent

## install: Install missing dependencies. Runs `go get` internally. e.g; make install get=github.com/foo/bar
install: go-get

deploy:	compile
	@bash -c "[ -n \"${VAX_RECIPIENT}\" ] || (echo '  ⚠️  VAX_RECIPIENT not set' && exit 1)"
	@bash -c "[ -n \"${VAX_MESSAGEBIRD_KEY}\" ] || (echo '  ⚠️  VAX_MESSAGEBIRD_KEY not set!' && exit 1)"
	@echo "  ⏳  Deploying function..."
	@gcloud functions deploy vaxalert --entry-point TriggerCheck --runtime go113 --trigger-http \
		--allow-unauthenticated --region=$(GCP_REGION) \
		--update-env-vars=VAX_RECIPIENT=${VAX_RECIPIENT},VAX_TARGET_YEAR=1995,VAX_MESSAGEBIRD_KEY=${VAX_MESSAGEBIRD_KEY},VAX_ENV=prod

## compile: Compile the binary.
compile:
	@-touch $(STDERR)
	@-rm $(STDERR)
	@-$(MAKE) -s go-compile 2> $(STDERR)
	@cat $(STDERR) | sed -e '1s/.*/\nError:\n/'  | sed 's/make\[.*/ /' | sed "/^/s/^/     /" 1>&2

## exec: Run given command, wrapped with custom GOPATH. e.g; make exec run="go test ./..."
exec:
	@GOPATH=$(GOPATH) GOBIN=$(GOBIN) $(run)

## clean: Clean build files. Runs `go clean` internally.
clean: go-clean

scheduler-create:
	@gcloud scheduler jobs create http $(SCHEDULER_NAME) \
		--schedule="*/5 6-22 * * *"
		--uri=$(gcloud functions describe $(FUNCTION_NAME) --region=$(GCP_REGION) --format='value(httpsTrigger.url)') \
		--http-method=GET \
		--timezone="Europe/Amsterdam"

scheduler-delete:
	@gcloud scheduler jobs delete $(SCHEDULER_NAME)

scheduler-pause:
	@gcloud scheduler jobs pause $(SCHEDULER_NAME)

scheduler-resume:
	@gcloud scheduler jobs resume $(SCHEDULER_NAME)

go-compile: go-clean go-get go-build

go-build:
	@echo "  ⏳  Building binary..."
	@GOPATH=$(GOPATH) GOBIN=$(GOBIN) go build -o $(GOBIN)/$(PROJECTNAME) $(GOFILES)

go-generate:
	@echo "  ⏳  Generating dependency files..."
	@GOPATH=$(GOPATH) GOBIN=$(GOBIN) go generate $(generate)

go-get:
	@echo "  ⏳  Checking if there is any missing dependencies..."
	@GOPATH=$(GOPATH) GOBIN=$(GOBIN) go get $(get)

go-install:
	@GOPATH=$(GOPATH) GOBIN=$(GOBIN) go install $(GOFILES)

go-clean:
	@echo "  ⏳  Cleaning build cache"
	@GOPATH=$(GOPATH) GOBIN=$(GOBIN) go clean

.PHONY: help
all: help
help: Makefile
	@echo
	@echo " Choose a command run in "$(PROJECTNAME)":"
	@echo
	@sed -n 's/^##//p' $< | column -t -s ':' |  sed -e 's/^/ /'
	@echo